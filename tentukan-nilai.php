<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=
    , initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    function tentukan_nilai($number){
        if ($number <= 100 && $number > 80){
            echo "nilai $number Sangat Baik <br>";
        } elseif ($number <= 80 && $number > 70) {
            echo "nilai $number Baik <br>";
        } elseif ($number <= 70 && $number > 60) {
            echo "nilai $number Cukup <br>";
        } elseif ($number <= 60) {
            echo "nilai $number Kurang <br>";
        }
    }
    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
?>
</body>
</html>